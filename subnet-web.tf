# INICIO - Subnet pública na AZ A, utilizado apenas para o Server Web, não usar para aplicação e BD, para isso utilizar subnet privada
resource "aws_subnet" "us_east_1a" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "Public Subnet us-east-1a"
  }
}
# FIM - Subnet pública na AZ A, utilizado apenas para o Server Web, não usar para aplicação e BD, para isso utilizar subnet privada

# INICIO - Subnet pública na AZ B, utilizado apenas para o Server Web, não usar para aplicação e BD, para isso utilizar subnet privada
resource "aws_subnet" "us_east_1b" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "Public Subnet us-east-1b"
  }
}
# FIM - Subnet pública na AZ B, utilizado apenas para o Server Web, não usar para aplicação e BD, para isso utilizar subnet privada

# INICIO - Tabela de rota com acesso público, rota default para o IGW
resource "aws_route_table" "my_vpc_public" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my_vpc_igw.id
    }

    tags = {
        Name = "Public Subnets Route Table for My VPC Tweet"
    }
}
# FIM - Tabela de rota com acesso público, rota default para o IGW

# INICIO - Associando a tabela de rota com a subnet da AZ A e AZ B
resource "aws_route_table_association" "my_vpc_us_east_1a_public" {
    subnet_id      = aws_subnet.us_east_1a.id
    route_table_id = aws_route_table.my_vpc_public.id
}

resource "aws_route_table_association" "my_vpc_us_east_1b_public" {
    subnet_id      = aws_subnet.us_east_1b.id
    route_table_id = aws_route_table.my_vpc_public.id
}
# FIM - Associando a tabela de rota com a subnet da AZ A e AZ B