import json
import csv
import tweepy
import re

"""
INPUTS:
    consumer_key, consumer_secret, access_token, access_token_secret: códigos 
    dizendo ao twitter que estamos autorizados a acessar esses dados
    url para criar a aplicação https://apps.twitter.com
    hashtag_phrase: the combination of hashtags to search for
OUTPUTS:
    none, simplesmente salvar as informações do tweet em uma planilha
"""
# Criar a função do aplicativo que fará a coleta das hashtags
def search_for_hashtags(consumer_key, consumer_secret, access_token, access_token_secret, hashtag_phrase):
    
    # Criar a autenticação para acessar o Twitter
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # Iniciando o Tweepy API
    api = tweepy.API(auth)
    
    # Nome da planilha onde será gravado as informações dos tweets
    fname = 'hashtags.csv'

    # Abrir a planilha CSV que será gravado as informações
    with open(fname, 'w') as file:
        w = csv.writer(file)

        # Escrever a linha de cabeçalho na planilha
        w.writerow(['Timestamp', 'Tweet Text', 'Username', 'All Hashtags', 'Followers Count'])

        # Para cada hashtag que será encontrada, será gravado na planilha em suas respectivas colunas
        for tweet in tweepy.Cursor(api.search, q=hashtag_phrase+' -filter:retweets', \
                                   lang="pt", tweet_mode='extended').items(5):
            w.writerow([tweet.created_at, tweet.full_text.replace('\n',' ').encode('utf-8'), tweet.user.screen_name.encode('utf-8'), [e['text'] for e in tweet._json['entities']['hashtags']], tweet.user.followers_count])

# Variáveis com as informações de autenticação

consumer_key = 'INFORMAR_A_CONSUMER_KEY'
consumer_secret = 'INFORMAR_A_CONSUMER_SECRET'
access_token = 'INFORMAR_A_ACCESS_TOKEN'
access_token_secret = 'INFORMAR_A_ACCESS_TOKEN_SECRET'

# Hashtags que serão pesquisadas no Twitter
hashtag_phrase = '#container'

# Chamando a função search_for_hashtags para começar a realizar o search
if __name__ == '__main__':
    search_for_hashtags(consumer_key, consumer_secret, access_token, access_token_secret, hashtag_phrase)

