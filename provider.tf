## Escolha do provider AWS com região "us-east-1", nos campos "access_key" e "secret_key" estão indicando que os valores estão salvos em outro arquivo
provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}