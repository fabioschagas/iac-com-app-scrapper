# INICIO - AWS Autoscaling Policy - Métrica UP no Cloudwatch
resource "aws_autoscaling_policy" "web_policy_up" {
  name                   = "web_policy_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
  alarm_name          = "web_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "60"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_description = "Essa métrica monitora a utilização de CPU da EC2 instance"
  alarm_actions = [aws_autoscaling_policy.web_policy_up.arn]
}
# FIM - AWS Autoscaling Policy - Métrica UP no Cloudwatch

# INICIO - AWS Autoscaling Policy - Métrica DOWN no Cloudwatch
resource "aws_autoscaling_policy" "web_policy_down" {
  name                   = "web_policy_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
  alarm_name          = "web_cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "60"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_description = "Essa métrica monitora a utilização de CPU da EC2 instance"
  alarm_actions     = [aws_autoscaling_policy.web_policy_down.arn]
}
# FIM - AWS Autoscaling Policy - Métrica DOWN no Cloudwatch

# Saída do IP do ELB
output "ELB_IP" {
  value = aws_elb.web_elb.dns_name
}
