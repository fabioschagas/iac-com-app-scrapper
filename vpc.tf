# INICIO - AWS VPC - Criando uma VPC
resource "aws_vpc" "my_vpc" {
  cidr_block            = "10.0.0.0/16"
  enable_dns_hostnames  = true
  
  tags = {
    Name = "VPC Tweet"
  }
}
# FIM - AWS VPC - Criando uma VPC