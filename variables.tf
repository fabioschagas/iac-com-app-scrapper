# Variáveis GLOBAIS:
variable "access_key" {}
variable "secret_key" {}

variable "region" {
  description = "Região na AWS onde os recursos serão criados."
  default     = "us-east-1"
}