# INICIO - AWS Security Group - Permitindo o tráfego
resource "aws_security_group" "elb_http" {
  name        = "elb_http"
  description = "Tráfego Allow HTTP de instância através do Elastic Load Balancer"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP ELB Security Group"
  }
}
# FIM - AWS Security Group - Permitindo o tráfego