# INICIO - AWS Internet Gateway - IGW
resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "My Tweet - Internet Gateway"
  }
}
# FIM - AWS Internet Gateway - IGW