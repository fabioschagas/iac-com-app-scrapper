# INICIO - AWS Launch Configuration - Escolhendo AMI, tipo da instância e uso simplificado do User Data
resource "aws_launch_configuration" "web" {
  name_prefix   = "web-"

  image_id      = "ami-02354e95b39ca8dec" # Amazon Linux 2 AMI, SSD Volume Type
  instance_type = "t2.micro"
  key_name      = "minha_chave"

  security_groups             = [aws_security_group.allow_http.id]
  associate_public_ip_address = true

  user_data = <<USER_DATA
#!/bin/bash
yum update
yum -y install nginx
echo "$(curl http://169.254.169.254/latest/meta-data/local-ipv4)" > /usr/share/nginx/html/index.html
chkconfig nginx on
service nginx start
  USER_DATA

  lifecycle {
    create_before_destroy = true
  }
}
# FIM - AWS Launch Configuration - Escolhendo AMI, tipo da instância e uso simplificado do User Data